<?php 
    class User extends Connect{

        public function login(){
            $connect=parent::connection();
            parent::set_name();

            if(isset($_POST["submit"])){
                $email = $_POST["user_email"];
                $pass = $_POST["user_pass"];

                if(empty($email) and empty($pass)){
                    header("location:".connect::base_url()."index.php?m=2");
                    exit();
                }else{
                    $sql = "SELECT * FROM tb_users WHERE user_email=? and user_pass=? and status=1";
                    $stmt = $connect->prepare($sql);
                    $stmt->bindValue(1, $email);
                    $stmt->bindValue(2, $pass);
                    $stmt->execute();
                    $result = $stmt->fetch();

                    if(is_array($result) and count($result)>0){
                        $_SESSION["user_id"]=$result["user_id"];
                        $_SESSION["user_fullname"]=$result["user_fullname"];
                        $_SESSION["user_nickname"]=$result["user_nickname"];
                        $_SESSION["user_email"]=$result["user_email"];
                        header("Location:".connect::base_url()."view/Home/");
                        exit();
                    }else{
                        header("Location:".connect::base_url()."index.php?m=1");
                        exit();
                    }
                }
            }
        }
    }