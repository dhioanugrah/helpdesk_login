<?php
    require_once("../../config/Connect.php");
    if(isset($_SESSION["user_id"])){
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Blank Page</title>
  <?php require_once("../LayoutPartial/link.php");?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

    <!--header -->
    <?php require_once("../LayoutPartial/header.php"); ?>

    <!-- navbar -->
    <?php require_once("../LayoutPartial/nav.php"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
   
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Tiket Konsultasi</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
          
          </div>
        </div>
        <div class="box-body">
          Start creating your amazing application!
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Footer
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
   
</div>
    <!-- js -->
    <?php require_once("../LayoutPartial/script.php"); ?>
    <script src="tiketkonsultasi.js" type="text/javascript"></script>
</body>
</html>
<?php
    }else{
        header("Location: ".Connect::base_url()."index.php");
    }
?>