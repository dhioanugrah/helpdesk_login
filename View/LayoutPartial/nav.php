<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">

        <li>
          <a href="..\Home\">
            <i class="fa fa-home"></i> <span>Home</span>
          </a>
        </li>

        <li>
          <a href="..\TiketKonsultasi\">
            <i class="fa fa-laptop"></i> <span>Konsultasi</span>
          </a>
        </li>

        <li>
          <a href="..\TiketBaru\">
            <i class="fa fa-book"></i> <span>Tiket Baru</span>
          </a>
        </li>
       
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>