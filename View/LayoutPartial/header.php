<header class="main-header">
    <!-- Logo -->
    <a href="../../public/index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>P</b>LI</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Perjalanan</b>Lindungi</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="../../public/img/user2-160x160.jpg" class="user-image" alt="User Image">
              <span>
                    <?php echo $_SESSION["user_fullname"];?>
                </span>
              <input type="hidden" id="user_id" value="<?php echo $_SESSION["user_id"]?>">  
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="../../public/img/user2-160x160.jpg" class="img-circle" alt="User Image">
               
                <p>
                    <?php echo $_SESSION["user_fullname"];?>
                    <small> <?php echo $_SESSION["user_email"];?></small>
                </p>
              </li>
             
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="../Logout/logout.php" class="btn btn-default btn-flat">Sign out</a>
                </div>
                <div class="text-center">
                  <a href="#" class="btn btn-default btn-flat">Setting</a>
                </div>
              </li>
            </ul>
          </li>
        
        </ul>
      </div>
    </nav>
  </header>